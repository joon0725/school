using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Move : MonoBehaviour
{
    public void SceneChange()
    {
        // 데스티네이션 스트링 변수에 현재 클릭된 오브젝트의 이름을 저장
        var destination = EventSystem.current.currentSelectedGameObject.name;
        // 만약 그 이름이 To로 시작한다면
        if (destination.StartsWith("To"))
        {
            // 3번째 글자부터 스트링을 만든다.
            destination = destination[2..];
        }
        //그 스트링에 해당하는 씬으로 이동한다.
        SceneManager.LoadScene(destination);
    }
}