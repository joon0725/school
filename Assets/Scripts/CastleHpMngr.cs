using CharacterTypes;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CastleHpMngr : MonoBehaviour
{
    public Slider myHpBar;
    public Slider enemyHpBar;
    private BaseCharacter _me;
    private BaseCharacter _enemy;
    void Start()
    {
        // 내 진영의 성을 _me에 저장
        _me = GameObject.Find("Castle").GetComponent<BaseCharacter>();
        // 상대 진영의 성을 _enemy에 저장
        _enemy = GameObject.Find("EnemyCastle").GetComponent<BaseCharacter>();
    }
    void Update()
    {
        // 내 hp바의 값을 내 hp와 같게 한다.
        myHpBar.value = _me.Hp;
        // 적 hp바의 값을 적의 hp와 같게 한다.
        enemyHpBar.value = _enemy.Hp;
        // 만일 내 hp 바의 값이 0보다 작다면
        if (myHpBar.value <= 0)
        {
            // 패배 화면으로 이동
            SceneManager.LoadScene("Lose");
        }
        // 적의 hp 바의 값이 0보다 작다면
        else if (enemyHpBar.value <= 0)
        {
            //승리 화면으로 이동
            SceneManager.LoadScene("Win");
        }
    }
}
