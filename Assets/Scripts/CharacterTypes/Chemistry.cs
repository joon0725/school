namespace CharacterTypes
{
    public class Chemistry : BaseCharacter
    {
        protected override int Period { get; set; } = 1;
        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            switch (Collided)
            {
                case Biology:
                case Information:
                    tempDmg *= 2;
                    break;
                case Physics:
                case Math:
                    tempDmg -= 5;
                    break;
            }

            Collided.Hp -= tempDmg;
        }
    }
}