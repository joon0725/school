namespace CharacterTypes
{
    public class Humanity : BaseCharacter
    {
        public override int Hp { get; set; } = 120;

        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            Collided.Hp -= tempDmg;
        }
    }
}