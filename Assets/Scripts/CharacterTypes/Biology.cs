namespace CharacterTypes
{
    public class Biology : BaseCharacter
    {
        public override int Hp { get; set; } = 120;

        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            switch (Collided)
            {
                case Earth:
                case Information:
                    tempDmg *= 2;
                    break;
                case Chemistry:
                case Math:
                    tempDmg -= 5;
                    break;
            }

            Collided.Hp -= tempDmg;
        }
    }
}