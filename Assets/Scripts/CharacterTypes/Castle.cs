using System.Collections;
using UnityEngine;

namespace CharacterTypes
{
    public class Castle : BaseCharacter
    {
        
        protected override void OnAttack() {}
        public override int Hp { get; set; } = 1000;
        protected override int Dmg { get; set; } = 0;

        protected override IEnumerator Cor()
        {
            while (true)
            {
                if (Collided is null) continue;
                Hp += (Hp == 1000) ? 0 : 1;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}