namespace CharacterTypes
{
    public class Information : BaseCharacter
    {
        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            switch (Collided)
            {
                case Math:
                    tempDmg *= 2;
                    break;
            }

            Collided.Hp -= tempDmg;
        }
    }
}