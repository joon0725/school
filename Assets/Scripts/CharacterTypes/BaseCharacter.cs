using System.Collections;
using UnityEngine;
using static System.Linq.Enumerable;
// 모든 오브젝트의 기초
namespace CharacterTypes
{
    public abstract class BaseCharacter : MonoBehaviour
    {
        // 체력, 공력력, 공격주기, 초기화
        public virtual int Hp { get; set; } = 100;
        protected virtual int Dmg { get; set; } = 10;
        protected virtual int Period { get; set; } = 2;
        protected abstract void OnAttack();

        // 코루틴 사용
        protected virtual IEnumerator Cor()
        {
            while (true)
            {
                // 나와 부딪힌 객체가 없다면,
                if (Collided is not null)
                {
                    // 공격
                    OnAttack();
                    // 주기만큼 대기
                    yield return new WaitForSeconds(Period);
                }
                else
                    foreach (var x in Range(0, 5))
                    {
                        //5번 반복하며 0.1초 간격으로 앞으로 0.2칸 이동
                        this.transform.position += new Vector3(0.2f, 0, 0);

                        yield return new WaitForSeconds(0.1f);
                    }
            }
        }

        private void Start()
        {
            StartCoroutine(Cor());
        }

        private void Update()
        {
            // 체력이 0 이하이면 오브젝트 삭제
            if(this.Hp <= 0)
                Destroy(gameObject);
        }

        private void OnDestroy()
        {
            // 모든 코루틴 종료
            StopAllCoroutines();
        }

        // 충돌 가능한 객체로 선언
        protected BaseCharacter Collided = null;
        private void OnCollisionStay2D(Collision2D collision)
        {
            Collided = collision.otherRigidbody.gameObject.GetComponent<BaseCharacter>();
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            Collided = null;
        }
        
    }

}