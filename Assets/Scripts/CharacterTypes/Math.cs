namespace CharacterTypes
{
    public class Math : BaseCharacter
    {
        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            switch (Collided)
            {
                case Physics:
                case Chemistry:
                case Biology:
                case Earth:
                    tempDmg *= 2;
                    break;
                case Information:
                    tempDmg -= 5;
                    break;
            }

            Collided.Hp -= tempDmg;
        }
    }
}