namespace CharacterTypes
{
    public class Earth : BaseCharacter
    {
        public override int Hp { get; set; } = 110;
        protected override int Dmg { get; set; } = 25;

        protected override void OnAttack()
        {
            int tempDmg = this.Dmg;
            switch (Collided)
            {
                case Physics:
                case Information:
                    tempDmg *= 2;
                    break;
                case Biology:
                case Math:
                    tempDmg -= 5;
                    break;
            }

            Collided.Hp -= tempDmg;
        }
    }
}