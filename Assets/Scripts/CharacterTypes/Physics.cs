using System;
using UnityEngine;

namespace CharacterTypes
{
    public class Physics : BaseCharacter
    {
        public Animator animator;
        protected override void OnAttack()
        {
            int tempDmg = Dmg;
            switch (Collided)
            {
                case Chemistry:
                case Information:
                    tempDmg *= 2;
                    break;
                case Earth:
                case Math:
                    tempDmg -= 5;
                    break;
            }

            Collided.Hp -= (tempDmg+10);
        }

        private void Awake()
        {
            if (this.Hp <= 0)
                Destroy(gameObject);
        }
    }
}