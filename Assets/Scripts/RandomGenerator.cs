using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class RandomGenerator : MonoBehaviour
{
    private Random _random;
    public string[] random = {"Phy", "Che", "Bio", "Earth", "Info", "Math", "Lit"};
    public List<int> cho = new List<int>();
    public int nowDate;
    private void Start()
    {
        // 현재 시각의 초 저장
        nowDate = DateTime.Now.Second;
        // 랜덤한 0부터 6까지의 수 중 겹치지 않게 3개를 뽑는 코드
        while (cho.Count < 3)
        {
            var t = _random.Next(0, 6);
            if (!cho.Contains(t))
                cho.Add(t);
        }
    }
    
    private void Awake()
    {
        // 5초에 한번
        if (DateTime.Now.Second - nowDate >= 5)
        {
            // 적을 랜덤한 라인에 랜덤한 타입으로 생성한다.
            GetComponent<EnemySpawn>().SpawnCharacter(_random.Next(0, 2), random[cho[_random.Next(0, 2)]]);
            nowDate = DateTime.Now.Second;
        }
    }
}
