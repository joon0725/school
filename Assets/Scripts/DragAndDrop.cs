using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//마우스 인식을 위한 클래스들을 상속받는다.
public class DragAndDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Camera _camera;
    private GameObject _spawn;
    private bool _isDragging;
    public int nowDate;
    public DragAndDropContainer dragNdrop;

    private void Start()
    {
        // SpawnPos 객체와 카메라 객체를 저장
        _spawn = GameObject.Find("SpawnPos");
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    //드래그가 시작될 때
    public void OnBeginDrag(PointerEventData eventData)
    {
        _isDragging = true;
        // 드래그 앤 드롭 컨테이너를 활성화
        dragNdrop.gameObject.SetActive(true);
        //드래그앤 드롭 컨테이너의 스프라이트를 내가 클릭되었으므로 나의 이미지의 스프라이트로 바꾼다.
        dragNdrop.GetComponent<SpriteRenderer>().sprite = GetComponent<Image>().sprite;
    }
    
    // 드래그가 진행 중일때
    public void OnDrag(PointerEventData eventData)
    {
        // 마우스 좌표를 월드 좌표계로 변환한다.
        eventData.position = _camera.ScreenToWorldPoint(eventData.position);
        // 드래깅 중이면,
        if (_isDragging)
            // 드래그 앤 드롭 컨테이너의 좌표를 내 마우스의 좌표로 이동시킨다.
            // 이때 내 드래그 앤 드롭 컨테이너의 좌표계와 내 마우스의 좌표계 모두 월드 좌표계를 따른다.
            dragNdrop.transform.position = eventData.position;
    }
    
    // 드래그가 끝났을 때
    public void OnEndDrag(PointerEventData eventData)
    {
        nowDate = DateTime.Now.Second;
        if (_isDragging)
            _isDragging = false;
        // i가 -2, 0, 2일 때, 드래그 앤 드롭 컨테이너의 y좌표가 각각의 라인의 범주 안에 있으면 
        for (var i = -2; i < 3; i += 2)
        {
            if (dragNdrop.transform.position.y < i + 1 && dragNdrop.transform.position.y > i - 1)
            {
                // Spawn 클래스의 스폰캐릭터 함수에 라인과 캐릭터 정보를 넘겨주어 생성시킨다. 
                _spawn.GetComponentInChildren<Spawn>().SpawnCharacter(i, DataMng.chosen[Int32.Parse(this.name)]);
                break;
            }
        }
        //드래그앤 드롭 컨테이너를 다시 원점으로 이동시키고, 비활성화 시킨다.
        dragNdrop.transform.position = new Vector3(0, 0, 0);
        dragNdrop.gameObject.SetActive(false);
        //2초간 기다린다.
        while ((DateTime.Now.Second - nowDate)%60 <= 2)
            continue;
    }
}