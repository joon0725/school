using UnityEngine;
using UnityEngine.UI;

public class CharMngr : MonoBehaviour
{
    // 버튼들을 리스트로 받으면서
    public GameObject[] buttons;
    
    private void Start()
    {
        // 각각의 버튼에 그 전 화면에서 선택한 캐릭터의 사진을 넣는다
        for (int i = 0; i < 3; i++)
            buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>(DataMng.chosen[i]);
    }
}