using System;
using CharacterTypes;
using UnityEngine;
using Math = CharacterTypes.Math;
using Physics = CharacterTypes.Physics;

public class Spawn: MonoBehaviour
{
    private Camera _camera;
    public void SpawnCharacter(int yPos, string g)
    {
        // 이 스크립트가 실행되고 있는 오브젝트를 복사하고 -7, 매개변수 yPos, 0의 위치에 생성한다.
        var ch = Instantiate(gameObject, new Vector3(-7, yPos, 0),
            transform.rotation);
        // switch case 문의 길이를 줄이기 위해 이러한 서술을 택함
        // 매개변수 g에 따라 해당하는 클래스를 복사한 객체에 추가한다.
        BaseCharacter _ = g switch
        {
            "Phy" => ch.gameObject.AddComponent<Physics>(),
            "Che" => ch.gameObject.AddComponent<Chemistry>(),
            "Bio" => ch.gameObject.AddComponent<Biology>(),
            "Earth" => ch.gameObject.AddComponent<Earth>(),
            "Math" => ch.gameObject.AddComponent<Math>(),
            "Info" => ch.gameObject.AddComponent<Information>(),
            "Lit" => ch.gameObject.AddComponent<Humanity>(),
            _ => throw new ArgumentOutOfRangeException(nameof(g), g, null)
        };
        // 복사한 객체의 사진을 매개변수 g를 파일명으로 갖는 이미지로 한다.
        ch.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>($"{g}");
    }
}
