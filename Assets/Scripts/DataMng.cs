using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class DataMng : MonoBehaviour
{
    // 비활성화 된 오브젝트에 접근하기 위해서 퍼블릭으로 선언하고 유니티 창에서 드래그 앤 드롭을 이용하여 오브젝트를 할당해 주었다.
    public GameObject n;
    // chosen이라는 이름의 스트링 리스트를 정적 공용으로 선언하고 초기화한다. 정적으로 선언한 이유는 모든 씬에서 사용하기 위함이다.
    public static List<string> chosen = new List<string>() {};
    // 클릭한 오브젝트를 담을 변수 선언
    public GameObject clickObject;
    // 객체의 갯수인 t 선언
    private int _t = 0;
    public void Add()
    {
        // 내가 클릭한 오브젝트를 clickObject에 저장
        clickObject = EventSystem.current.currentSelectedGameObject;
        // 만일 t가 3보다 작고, 내가 클릭한 오브젝트가 아직 추가되지 않았다면,
        if (_t < 3 && !chosen.Contains(clickObject.name))
        {
            // 리스트에 내가 선택한 오브젝트 삽입
            chosen.Add(clickObject.name);
            _t++;
        }
    }
}