using System;
using CharacterTypes;
using UnityEngine;
using Math = CharacterTypes.Math;
using Physics = CharacterTypes.Physics;

// Spawn 코드와 생성 좌표 외에 모두 일치하므로 주석 생략
public class EnemySpawn: MonoBehaviour
{
    private Camera _camera;
    public void SpawnCharacter(int yPos, string g)
    {
        var ch = Instantiate(gameObject, new Vector3(7, yPos, 0),
            transform.rotation);
        BaseCharacter c = g switch
        {
            "Phy" => ch.gameObject.AddComponent<Physics>(),
            "Che" => ch.gameObject.AddComponent<Chemistry>(),
            "Bio" => ch.gameObject.AddComponent<Biology>(),
            "Earth" => ch.gameObject.AddComponent<Earth>(),
            "Math" => ch.gameObject.AddComponent<Math>(),
            "Info" => ch.gameObject.AddComponent<Information>(),
            "Lit" => ch.gameObject.AddComponent<Humanity>(),
            _ => throw new ArgumentOutOfRangeException(nameof(g), g, null)
        };
        ch.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>($"{g}");
    }
}